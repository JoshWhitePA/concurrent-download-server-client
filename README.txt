#Author:Joshua White
#Creation date: November 10, 2015
#Professor: Lisa Frye
#Class:CSC 328
#Due: November 30, 2015
#Using Python 2.6 interpreter on CentOS 6.6
The purpose of the programs Server.py and Client.py is to allow a user to download files remotely from a server
onto their own client machine. The server accepts 4 basic commands
• BYE – sent by client on disconnect
• DIR – directory listing
• CD – client change directory
• DOWNLOAD – client downloads requested file

If there is a file in the clients current directory with the same filename, the user will be given the option to overwrite the file

Decision: How will the server indicate the end of the directory listing?
	I used a deleimiter to show the client when the listing would end. I showed what was a directory and what was a file by placeing the word
	file or dir before each listing.

Decision: What will server response be for successful and unsuccessful cd commands?
	A successfull CD command will reply back to the client with threi new current working directory, ad failed command will send back the
	exception that was thrown by the inerpreter which will state errors like permission denied.

Decision: How does the client know when the server has completed sending the file?
	Again, I attach the Delimiter "|-o-|"(Tie Fighter symbol) to the client to tell it that it is done reading.

One issue that I had was joining threads because I used the thread.start_new_thread which gives me no option to join the thread after use.
I checked the python servers thread count with ps -T -p (serverPID) and found that when I close the client connection the thread disapears from the list. A better choice would have been to use the newer threading library but the one I used had the best conncurrent server examples.

I was unable to have the client socket timeout if it lost connection to the server. By default the recv and send calls are blocking calls and will not raise an exception if a timeout is met. The exception can be raised only if those calls are made non-blocking, even after trying this method I was unable to get the timeout exception to be raised, it may be due to the fact that we are still running python 2.6 on the server.
