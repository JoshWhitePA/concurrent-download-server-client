#!/bin/python
#Author:Joshua White
#Creation date: November 10, 2015
#Professor: Lisa Frye
#Class:CSC 328
#Due: November 30, 2015
#Using Python 2.6 interpreter on CentOS 6.6
# This python server allows the client to navigate though filesystem and download files, the program will display error as necessary
#
#


from socket import *
import errno
import thread, os, sys

BUFF = 1024
HOST = '127.0.0.1'
PORT = 55201
#folders being listed as files

#Reads data from socket until delimiter is found and then returns the message in a string format
def readDumbSocket(clientsocket):
	while 1:
		data = s.recv(BUFFER_SIZE)
		if "|-o-|" in data:
			data = data.replace("|-o-|","")
			msg += data
			break
	return msg

#The function that the new thread runs
def threadClient(clientsock, addr):
	path = os.getcwd()# set the current directory string
	clientsock.send(response("HELLO"))#sends welcome message
	clientsock.send("|-o-|")
	arg = ["none",""]
	msg = ""
	inpt = ""
	data = ""
	while arg[0].upper() != "BYE":
		while 1:
			data = clientsock.recv(BUFF)
			msg += data
			if "|-o-|" in data:
				msg = msg.replace("|-o-|","")
				break
		arg = msg.split()
		if len(arg) == 0:#in case of an empty message, will stop index out of range for arg list
			arg = [" "," "]

		if arg != None:
			if arg[0].upper() == "DIR":#Client wants directory listed
				getDirList(clientsock,path)
			elif arg[0].upper()  == "CD":#Client wants to change directory
				if len(arg) > 1:
					path = chCWD(path,clientsock,arg[1] )
				else:
					clientsock.send("CD needs an argument!\n|-o-|")
			elif arg[0].upper() == "DOWNLOAD":#client wants to download
				if len(arg) > 1:
					checkFileToSend(arg[1], clientsock,path)
				else:
					clientsock.send("Download needs an argument!\n|-o-|")
			elif arg[0].upper() == "BYE":#Strat ending thread
				break
			else:
				clientsock.send("Command not found!!!|-o-|")
			arg = ["none",""]
			data = None
			msg = ""
	clientsock.send("Connection Closed|-o-|")
	clientsock.close()
	print addr, "- closed connection"  # log on console

def response(key):
	return key



#changes current directory by returning a string with normalized path that is then used for file operations
# this was done because a process shares one global working directory among its threads
def chCWD(path, sock,placeToGo):
	basePath = path
	#path is an absolute path
	if os.path.isabs(placeToGo):
		path = placeToGo
	else:#relative path
		path = path+"/" + placeToGo

	if os.path.exists(path):#checks if created path exists, returns new path if true
		sock.send("Now in: " + os.path.normpath(path) + "|-o-|")
		return os.path.normpath(path)
	else:#return old path if false
		sock.send("Path: " + os.path.normpath(path) + " not found!|-o-|")
		return os.path.normpath(basePath)
	return os.path.normpath(basePath)

#send the file to the client
def sendFile(fileName, clientsock,cwd):
	with open(cwd+"/"+fileName) as f:
		for line in f:
			clientsock.send(response(line))
	clientsock.send(response("|-o-|"))

#checks to see if file exists in the current working directory
def checkFileToSend(fileName, clientsock, cwd):
	data = ""
	if os.path.isfile(cwd + "/"+fileName) == True:
		clientsock.send(response("READY|-o-|"))
	else:
		clientsock.send(response("File Not Found|-o-|"))
		return 1

	while 1:
		data += clientsock.recv(BUFF)
		if "|-o-|" in data:
			data = data.replace("|-o-|","")
			break
	if data == "READY":
		sendFile(fileName, clientsock,cwd)
	elif data == "STOP":
		clientsock.send(response("Download Not Started|-o-|"))
		return 1
	return 0

#list files and folders in directory
def getDirList(clientsock,cwd):
	try:
		dirList = os.listdir(cwd)
		msg='\n'
		for fOrD in dirList:
			if os.path.isdir(cwd+"/"+fOrD) == True:
				msg += "Dir: " + fOrD + "\n"
			else:
				msg += "File: " + fOrD + "\n"
	except OSError as e:
		msg = str(e)
	clientsock.send(msg + "|-o-|")

if __name__ == '__main__':
	if len(sys.argv) == 3:
		print "Usage: python Server.py <Port>"
		sys.exit(0)
	elif len(sys.argv) == 2:
		PORT = int(sys.argv[1])
	ADDR = ("127.0.0.1", PORT)
	#create socket connection
	try:
		serversock = socket(AF_INET, SOCK_STREAM)
		serversock.setsockopt(SOL_SOCKET, SO_REUSEADDR, 1)
		serversock.bind(ADDR)
		serversock.listen(5)
		while 1:
			print 'Listening on port', PORT
			clientsock, addr = serversock.accept()
			print '...connected from:', addr
			thread.start_new_thread(threadClient, (clientsock, addr))
	except error as errmsg:
		print "Error in socket:" + str(errmsg)
		sys.exit(1)
	except KeyboardInterrupt:
		print "\nExited Safely"
		serversock.close()
		sys.exit(1)
