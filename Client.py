#!/bin/python
#Author:Joshua White
#Creation date: November 10, 2015
#Professor: Lisa Frye
#Class:CSC 328
#Due: November 30, 2015
#Using Python 2.6 interpreter on CentOS 6.6
# This program connects to a server concurrently, allows the user to navigate through the filesystem
# and download a file. To exit the program type bye in the client.

import socket, os, sys, atexit

#Read from the socket, so named because of the frustration this caused me
def readDumbSocket(s):
	msg = ""
	try:
		while 1:
			data = s.recv(BUFFER_SIZE)
			msg += data
			if "|-o-|" in data:
				msg = msg.replace("|-o-|", "")
				break
		#print msg
	except KeyboardInterrupt:
		s.send("BYE|-o-|")
		msg = readDumbSocket(s)
		print msg
		s.close()
		sys.exit(1)
	except socket.error, e:
		print "Strange error receiving or sending from socket: %s" % e
		s.close()
		sys.exit(1)
	return msg

#handles the file download by checking if file exists and should be overwritten.
def downloadHandler(s, splitStuff):
	overWrite = ""
	splitStuff = inpt.split()
	if not os.path.isfile(splitStuff[1]):
		s.send("READY|-o-|")
		rcvFile(splitStuff[1], s)
	else:
		tf = getYN('File Already Exists, do you wish to overwrite?(y/n): ')
		if tf:
			s.send("READY|-o-|")
			rcvFile(splitStuff[1], s)
		else:
			s.send("STOP|-o-|")
			msg = readDumbSocket(s)
			print msg
			return 0

#reads the file from socket and writes it to created file
def rcvFile(fileName, s):
	print "Filename Downloaded: " + fileName
	msg = readDumbSocket(s)
	writeToMe = open(fileName, "w+", 0)
	writeToMe.write(msg)
	writeToMe.close()

#prompt for overwriting of the file
def getYN(prompt):
	answer = raw_input(prompt)
	while answer.upper() != "Y" and answer.upper() != "N":
		answer = raw_input(prompt)
	if answer.upper() == "Y":
		return True
	else:
		return False


TCP_IP = '127.0.0.1'
TCP_PORT = 55201;
if len(sys.argv) != 3:
	print "Usage: python Client.py <IP Address> <Port>"
	sys.exit(0)
TCP_IP = sys.argv[1]
TCP_PORT = int(sys.argv[2])

BUFFER_SIZE = 1024
inpt = ""
msg = ""
data = ""
prevInpt = ""
splitStuff = []
counter = 0
#create socket and read from it
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.settimeout(5.0)
try:
	s.connect((TCP_IP, TCP_PORT))
	while inpt != "Quit":
		msg = readDumbSocket(s)
		if msg != "READY":
			print msg
		if msg == "Connection Closed":
			s.close()
			sys.exit(0)
		if msg == "READY":
			downloadHandler(s, splitStuff)
		inpt = raw_input('Enter your input:')
		prevInpt = inpt
		s.send(inpt + "|-o-|")
		msg = ""
		data = ""
except socket.error, e:
	print "Strange error receiving or sending from socket: %s" % e
	s.close()
	sys.exit(1)
except KeyboardInterrupt:
	s.send("BYE|-o-|")
	msg = readDumbSocket(s)
	print msg
	s.close()
	sys.exit(1)
except socket.timeout, err:
	print "Server Timed out: %s" % err
	s.close()
	sys.exit(1)
print "Client Closed"
s.close()
sys.exit(0)
